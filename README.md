# README #

## The application ##

Welcome to the EuVIDE site. This repository contains the latest fully-working version of Judith's IDE - a Form Designer and testbed for developing GUI applications in Euphoria, using the win32lib library for MS Windows operating systems. It has been tested on all Operating Systems from MS Win95 to MS Windows 10. It is currently a 32-bit application but works on 64-bit machines also.

### What is in this repository? ###

* Version 1.0.6 of the Form Designer, based on the win32lib extension library module
* The application is used for designing GUI solutions, using the Form Designer, which includes built-in code and menu editors
* This version is not dependent in any particular version of Euphoria

### How do I get set up? ###

* Clone the repository or download the zip file and unzip it to a folder of your choice
* This is the executable version 1.0.6
* It is suggested that you leave the sub-folder structure as is
* The IDE uses a project file to record all the details of each and every design; projects can generate either GUI applications (*.exw) or support library modules (.ew)
* You can alternate between the two forms for testing purposes, before finalising the design
* You can save your project files (*.prj) anywhere, but it is recommended that you locate each one in the same folder as your application or library module
* (Left) double-clicking is the normal mode of running the IDE
* Although the IDE application does not require any modules in addition to those included in this distribution, you do need a working copy of the Win32Lib library to use the IDE to develop your own working solutions. This library should be located in a suitable place in your folder structure so that it is recognisable either through the environmental variable **euinc** (any version of Euphoria) or through the **eu.cfg** file (specifically Open Euphoria 4).

### Contribution guidelines ###

Any help would be appreciated in any of the following area:

* Writing test examples
* Writing code review
* Writing a tutorial or other educative material

### Who do I talk to? ###

* Repository owner is C A Newbould (Bitbucket user: CANewbould)
* For details of Open Euphoria see [this site](http://openeuphoria.org/index.wc)

### **** NEW **** ###

Why not try out the tutorial, which will soon follow!
